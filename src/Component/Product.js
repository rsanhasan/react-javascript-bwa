import React from 'react';
import PropTypes from 'prop-types';

import '../App.css';

function CheckDiscount(props){
    const {isDiscount, discount } = props;
    if (isDiscount){
      return(
      <p>Diskon {discount}% Off.</p>
      )
    }else{
      return(
        <p>Belum ada diskon.</p>
      )
    }
  }

function Product(props) {
    const {category, title, isDiscount} = props;
    const techSpecs = [
      {
        "id": 1,
        "feature": "Ultralight 12.5inch FHD display"
      },
      {
        "id": 2,
        "feature": "Just under 3 pounds"
      },
      {
        "id": 3,
        "feature": "Fast Processing, plenty of storage and memory"
      },
      {
        "id": 4,
        "feature": "All day battery life"
      },
      {
        "id": 5,
        "feature": "Plus with our legendary ThinkPad support"
      }
    ];
    const listFeatures = techSpecs.map((listFeature) => 
      <li key={listFeature.id}>{listFeature.feature}</li>
    );
    return (
      <div>
        <div className="Deskripsi">
          <p className="Category">{category}</p>
          <h1 className="Title">{title}</h1>
          <p className="Price">IDR 12.500.000</p>
          <CheckDiscount isDiscount={isDiscount} discount={50}/>
          <p className="Info">
          Performance You Can Count On With the responsiveness and efficiency of 6th generation Intel® Core™ processors, you can switch effortlessly between your favorite apps. What’s more, with a solid-state drive (SSD)—unlike with a regular mechanical hard drive—there are no moving parts and it performs faster. Boot up time is nearly three times quicker, while opening files is up to 30% faster. Even transferring files takes significantly less time. So in addition to delivering powerful performance, the X270 is quieter and more reliable.
          </p>
          <ul>
            {listFeatures}
          </ul>
          <a onClick={(e) => TambahCart(title, e)} href="#">Add to Cart</a>
        </div>
      </div>    
    )
  
  }

  function TambahCart(e){
    console.log(`Membeli produk ${e}`)
  }

  CheckDiscount.propTypes = {
    discount: PropTypes.number.isRequired
  }
  

  export default Product;