import React from 'react';
import '../App.css';

function Reviews(){
    const users = [
      {
        "id": 1,
        "name": "Christopher",
        "review": "The ThinkPad X270 is lightweight, rugged and loaded with ports. X270 IPS display is gorgeous, bright and easy on the eyes.",
        "photo": "https://images.pexels.com/photos/1516680/pexels-photo-1516680.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
      },
      {
        "id": 2,
        "name": "Sasha",
        "review": "The ThinkPad X270 is lightweight, rugged and loaded with ports. X270 IPS display is gorgeous, bright and easy on the eyes.",
        "photo": "https://images.pexels.com/photos/1458332/pexels-photo-1458332.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
      },
      {
        "id": 3,
        "name": "Stevie",
        "review": "The ThinkPad X270 is lightweight, rugged and loaded with ports. X270 IPS display is gorgeous, bright and easy on the eyes.",
        "photo": "https://images.pexels.com/photos/2120114/pexels-photo-2120114.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
      }
    ];
  
    const listReviews = users.map((itemReview) =>
      <div  key={itemReview.id} className="Item">
        <img src={itemReview.photo}/>
        <div className="User">
          <h3>{itemReview.name}</h3>
          <p>{itemReview.review}</p>
        </div>
      </div>
    );
  
    return (
      <div className="ReviewBox">
        <h2>Reviews</h2>
        {listReviews}
      </div>
    )
  };

export default Reviews;