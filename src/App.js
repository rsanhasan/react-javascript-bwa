import React from 'react';
import './App.css';

import Reviews from './Component/Reviews';
import Product from './Component/Product';
import PhotoProduct from './Component/PhotoProduct';

function App() {
  return (
    <div className="ParentBox">
      <PhotoProduct/>
      <Product
        category="X Series"
        title="Thinkpad X270"
        isDiscount="yes"/>
      <Reviews/>
    </div>

  );
}

export default App;